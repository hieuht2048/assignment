import { all, fork } from 'redux-saga/effects';
import { watchHomeSaga } from './watcher';

export default function* rootSaga() {
    yield all([fork(watchHomeSaga)]);
}
