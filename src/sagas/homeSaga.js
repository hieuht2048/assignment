import { call, put, select } from 'redux-saga/effects';
import services from '../services/apis';

import { getDataSuccess, getDataError, setClosePriceData } from '../actions/homeActions';
import { makeSelectTickerSymbol, makeSelectTimeRange } from '../selectors/homeSelectors';

export function* getDataSaga() {
    try {
        const tickerSymbol = yield select(makeSelectTickerSymbol());
        const timeRange = yield select(makeSelectTimeRange());
        const payload = {
            tickerSymbol,
            timeRange: timeRange.toJS(),
        };
        const data = yield call(services.getData, payload);
        if (data) {
            yield put(getDataSuccess(data));
            yield put(setClosePriceData(data.dataset.data));
        } else {
            yield put(getDataSuccess([]));
            yield put(setClosePriceData([]));
        }
    } catch (err) {
        const errorString = `${err.message}`;
        yield put(getDataError(errorString));
    }
}

export function* getXXXSaga() {
    // xxxx
}
