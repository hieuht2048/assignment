import { takeLatest } from 'redux-saga/effects';
import { getDataSaga } from './homeSaga';
import * as types from '../constants/actionTypes';

export function* watchHomeSaga() {
    yield takeLatest(types.GET_DATA_REQUEST, getDataSaga);
}
