import React, { Component } from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { ButtonBase, Grid } from '@material-ui/core';
import { getData, setTicketSymbol, setTimeRange, setClosePriceData } from '../../actions/homeActions';

import { makeSelectTickerSymbol, makeSelectLoading, makeSelectData, makeSelectClosePriceData } from '../../selectors/homeSelectors';

import Chart from './Chart';
import utils from '../../utils/utils';

class HomePage extends Component {
    componentDidMount() {
        const { onGetData } = this.props;
        onGetData(); // get data default
    }

    onGetDataByRange = range => {
        const { onGetData, onSetTimeRange } = this.props;
        let timeRange = '';
        let end_date = new Date();
        const btns = document.getElementsByClassName('btns');
        const btnActived = document.getElementById(range);

        // use old style of "for loop" to fix the bad browser: Microsoft Edge
        for (let i = 0; i < btns.length; i++) {
            btns[i].classList.remove('actived');
            btnActived.classList.add('actived');
        }

        end_date = utils.formatDate(end_date);
        switch (range) {
            case '1w':
                {
                    let start_date = new Date();
                    const pastDate = start_date.getDate() - 7;
                    start_date.setDate(pastDate);
                    start_date = utils.formatDate(start_date);
                    timeRange = {
                        start_date,
                        end_date,
                    };
                }
                break;
            case '1m':
                {
                    let start_date = new Date();
                    start_date.setFullYear(start_date.getFullYear(), start_date.getMonth() - 1);
                    start_date = utils.formatDate(start_date);
                    timeRange = {
                        start_date,
                        end_date,
                    };
                }
                break;
            case '3m':
                {
                    let start_date = new Date();
                    start_date.setFullYear(start_date.getFullYear(), start_date.getMonth() - 3);
                    start_date = utils.formatDate(start_date);
                    timeRange = {
                        start_date,
                        end_date,
                    };
                }
                break;
            case '6m':
                {
                    let start_date = new Date();
                    start_date.setFullYear(start_date.getFullYear(), start_date.getMonth() - 6);
                    start_date = utils.formatDate(start_date);
                    timeRange = {
                        start_date,
                        end_date,
                    };
                }
                break;
            case '1y':
                {
                    let start_date = new Date();
                    start_date.setFullYear(start_date.getFullYear() - 1);
                    start_date = utils.formatDate(start_date);
                    timeRange = {
                        start_date,
                        end_date,
                    };
                }
                break;
            case '5y':
                {
                    let start_date = new Date();
                    start_date.setFullYear(start_date.getFullYear() - 5);
                    start_date = utils.formatDate(start_date);
                    timeRange = {
                        start_date,
                        end_date,
                    };
                }
                break;
            case 'all':
                timeRange = {
                    start_date: null,
                    end_date: null,
                };
                break;
            default: {
                let start_date = new Date();
                start_date.setFullYear(start_date.getFullYear() - 1);
                start_date = utils.formatDate(start_date);
                timeRange = {
                    start_date,
                    end_date,
                };
            }
        }
        onSetTimeRange(timeRange);
        onGetData();
    };

    getDataField = (data, field) => {
        const labels = [];
        for (let arr of data) {
            const label = arr[field];
            labels.push(label);
        }
        return labels;
    };

    onEnter = e => {
        const { onGetData } = this.props;
        const searchValue = e.target.value;
        if (e.key === 'Enter') {
            if (searchValue.length > 0) {
                onGetData();
                e.preventDefault();
            }
        }
    };

    onFindGreateClosePrice = e => {
        const { onSetGreaterClosePriceData, closePriceData, data } = this.props;
        if (e.target.value.length > 0) {
            const dataFiltered = closePriceData.filter(price => Number(price[1]) > Number(e.target.value));
            onSetGreaterClosePriceData(dataFiltered);
        } else {
            onSetGreaterClosePriceData(data.dataset.data);
        }
    };

    render() {
        const { onSearchInputTickerSymbol, loading, data, closePriceData } = this.props;
        let options = {};

        const defaultSettings = {
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            legend: {
                display: false,
            },
        };

        const nodataOptions = {
            labels: [],
            datasets: [
                {
                    label: '',
                    ...defaultSettings,
                    data: [],
                },
            ],
        };

        if (data.dataset) {
            if (data.dataset.data.length > 0) {
                options = {
                    labels: this.getDataField(data.dataset.data, 0),
                    datasets: [
                        {
                            label: 'USD',
                            ...defaultSettings,
                            data: this.getDataField(data.dataset.data, 1),
                        },
                    ],
                };
            } else {
                options = nodataOptions;
            }
        } else {
            options = nodataOptions;
        }

        const chartProps = { options, loading };
        const btns = [
            { name: '1 Week', value: '1w', default: false },
            { name: '1 Month', value: '1m', default: false },
            { name: '3 Months', value: '3m', default: false },
            { name: '6 Months', value: '6m', default: false },
            { name: '1 Year', value: '1y', default: true },
            { name: '5 Years', value: '5y', default: false },
            { name: 'All', value: 'all', default: false },
        ];

        return (
            <Grid container>
                <Grid container className="top">
                    <Grid item xs={4} className="search">
                        <input
                            defaultValue="FB"
                            type="text"
                            placeholder="Search..."
                            onChange={onSearchInputTickerSymbol}
                            onKeyPress={e => this.onEnter(e)}
                        />
                        <i className="fas fa-search" />
                    </Grid>
                    <Grid item xs={8} className="buttons">
                        {btns.map((btn, index) => {
                            return (
                                <ButtonBase
                                    key={index}
                                    id={btn.value}
                                    className={btn.default ? 'btn btn-normal btn-default btns actived' : 'btn btn-normal btn-default btns'}
                                    onClick={() => this.onGetDataByRange(btn.value)}
                                >
                                    {btn.name}
                                </ButtonBase>
                            );
                        })}
                    </Grid>
                </Grid>

                <Grid container className="main">
                    {data.dataset ? (
                        <Grid item xs={9} className="left">
                            <h1 className="left-title">
                                {data.dataset.data.length > 0 ? (
                                    data.dataset.name
                                ) : (
                                    <span className="warning">
                                        <i className="fas fa-exclamation-circle" />
                                        No Data Available! (Data only available from {data.dataset.oldest_available_date} to&nbsp;
                                        {data.dataset.newest_available_date})
                                    </span>
                                )}
                            </h1>
                            {data.dataset.data.length > 0 && <Chart {...chartProps} />}
                        </Grid>
                    ) : !loading ? (
                        <Grid item xs={9} className="left">
                            <h1 className="left-title">
                                <span className="warning">
                                    <i className="fas fa-exclamation-circle" />
                                    No Data Available! Please try again later.
                                </span>
                            </h1>
                        </Grid>
                    ) : (
                        <Grid item xs={9} className="left">
                            Waiting...
                        </Grid>
                    )}
                    <Grid item xs={3} className="right">
                        <Grid className="right-title">Find greater Close Prices</Grid>
                        <Grid className="right-filter">
                            <input type="number" placeholder="Enter a Close Price" onChange={e => this.onFindGreateClosePrice(e)} />
                            <i className="fas fa-filter" />
                        </Grid>

                        <Grid className="right-content">
                            {!loading &&
                                closePriceData &&
                                (closePriceData.length > 0 ? (
                                    <ul>
                                        {closePriceData.map((item, index) => (
                                            <li key={index}>
                                                {item[0]}
                                                <span>${item[1]}</span>
                                            </li>
                                        ))}
                                    </ul>
                                ) : (
                                    <span className="no-data">Have no any Close Price</span>
                                ))}
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

HomePage.propTypes = {
    tickerSymbol: PropTypes.string.isRequired,
    onSearchInputTickerSymbol: PropTypes.func.isRequired,
    onGetData: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    data: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
    onSetTimeRange: PropTypes.func.isRequired,
    closePriceData: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
    onSetGreaterClosePriceData: PropTypes.func.isRequired,
};

HomePage.defaultProps = {};

const mapStateToProps = createStructuredSelector({
    tickerSymbol: makeSelectTickerSymbol(),
    loading: makeSelectLoading(),
    data: makeSelectData(),
    closePriceData: makeSelectClosePriceData(),
});

const mapDispatchToProps = dispatch => {
    return {
        onSearchInputTickerSymbol: evt => {
            dispatch(setTicketSymbol(evt.target.value));
        },
        onSetTimeRange: timeRange => {
            dispatch(setTimeRange(timeRange));
        },
        onGetData: evt => {
            if (evt !== undefined && evt.preventDefault) evt.preventDefault();
            dispatch(getData());
        },
        onSetGreaterClosePriceData: closePriceData => {
            dispatch(setClosePriceData(closePriceData));
        },
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomePage);
