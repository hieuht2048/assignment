import React, { Component } from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Line } from 'react-chartjs-2';

import CircularProgress from '@material-ui/core/CircularProgress';
import { Grid } from '@material-ui/core';

class Chart extends Component {
    render() {
        const { options, loading } = this.props;
        const legend = {
            display: false,
        };
        return (
            <Grid className="chart">
                <div className={loading ? 'loading show' : 'loading hide'}>
                    <CircularProgress size={32} />
                </div>
                <div className={!loading ? 'chart show' : 'chart hide'}>
                    <Line legend={legend} data={options} />
                </div>
            </Grid>
        );
    }
}

Chart.propTypes = {
    loading: PropTypes.bool,
    options: PropTypes.object,
};

Chart.defaultProps = {
    loading: true,
    options: {},
};

const mapStateToProps = createStructuredSelector({});

const mapDispatchToProps = () => {
    return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chart);
