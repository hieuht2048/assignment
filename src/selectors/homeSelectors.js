import { createSelector } from 'reselect';

const homeSelect = state => {
    return state.homeReducer;
};

const makeSelectTickerSymbol = () =>
    createSelector(
        homeSelect,
        state => state.get('tickerSymbol')
    );

const makeSelectData = () =>
    createSelector(
        homeSelect,
        state => state.get('data')
    );

const makeSelectLoading = () =>
    createSelector(
        homeSelect,
        state => state.get('loading')
    );

const makeSelectTimeRange = () =>
    createSelector(
        homeSelect,
        state => state.get('timeRange')
    );

const makeSelectClosePriceData = () =>
    createSelector(
        homeSelect,
        state => state.get('closePriceData')
    );

export { makeSelectTickerSymbol, makeSelectData, makeSelectLoading, makeSelectTimeRange, makeSelectClosePriceData };
