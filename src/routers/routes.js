import AsyncComponent from '../components/AsyncComponent';

const HomePage = AsyncComponent(() => import('../components/home'));

const Routes = [
    {
        title: 'Home',
        path: '/',
        exact: true,
        component: HomePage,
    },
];

export default Routes;
