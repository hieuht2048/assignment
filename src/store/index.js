import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
//import logger from 'redux-logger';
import createBrowserHistory from 'history/createBrowserHistory';

import rootReducer from '../reducers';
import rootSaga from '../sagas';

export const browserHistory = createBrowserHistory();

//  Returns the store instance
// It can  also take initialState argument when provided
const configureStore = () => {
    const sagaMiddleware = createSagaMiddleware();
    return {
        ...createStore(rootReducer, applyMiddleware(sagaMiddleware)),
        runSaga: sagaMiddleware.run(rootSaga),
    };
};

export default configureStore;
