import * as types from '../constants/actionTypes';

export const getData = () => ({
    type: types.GET_DATA_REQUEST,
});

export const setTicketSymbol = tickerSymbol => ({
    type: types.SET_TICKER_SYMBOL,
    tickerSymbol,
});

export const getDataSuccess = data => ({
    type: types.GET_DATA_SUCCESS,
    data,
});

export const getDataError = getDataErr => ({
    type: types.GET_DATA_ERROR,
    getDataErr,
});

export const setTimeRange = timeRange => ({
    type: types.SET_TIME_RANGE,
    timeRange,
});

export const setClosePriceData = closePriceData => ({
    type: types.SET_CLOSE_PRICE_DATA,
    closePriceData,
});
