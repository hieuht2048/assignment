import { fromJS } from 'immutable';
import * as types from '../constants/actionTypes';
import Utils from '../utils/utils';

// Get timerange of the past one year
const getDefaultTimeRange = () => {
    let d = new Date();
    const pastYear = d.getFullYear() - 1;
    const fullY = d.setFullYear(pastYear);
    const start_date = Utils.formatDate(fullY);
    let ed = new Date();
    const end_date = Utils.formatDate(ed);
    return {
        start_date,
        end_date,
    };
};

const timeRange = getDefaultTimeRange();

const initialState = fromJS({
    data: [],
    loading: false,
    getDataErr: '',
    tickerSymbol: 'FB',
    timeRange,
    closePriceData: [],
});

export default function(state = initialState, action) {
    switch (action.type) {
        case types.SET_TICKER_SYMBOL:
            return state.set('tickerSymbol', action.tickerSymbol);
        case types.GET_DATA_REQUEST:
            return state.set('loading', true);
        case types.GET_DATA_SUCCESS:
            return state.set('data', action.data).set('loading', false);
        case types.GET_DATA_ERROR:
            return state.set('getDataErr', action.getDataErr).set('loading', false);
        case types.SET_TIME_RANGE:
            return state.set('timeRange', fromJS(action.timeRange));
        case types.SET_CLOSE_PRICE_DATA:
            return state.set('closePriceData', action.closePriceData);
        default:
            return state;
    }
}
