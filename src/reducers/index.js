import { combineReducers } from 'redux';
import homeReducer from './homeReducer';

// Combines all reducers to a single reducer function
const rootReducer = combineReducers({
    homeReducer,
});

export default rootReducer;
