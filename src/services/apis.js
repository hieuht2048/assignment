import axios from 'axios';

const apiKey = 'kXiYrraBf-Me71E-oC-5';

const getEnpoint = payload => {
    ///?column_index=4&start_date=2014-01-01&end_date=2014-12-31&collapse=monthly&transform=rdiff&api_key=YOURAPIKEY"
    if (!payload.timeRange.start_date) {
        return `https://www.quandl.com/api/v3/datasets/WIKI/${payload.tickerSymbol}.json?column_index=4&api_key=` + apiKey;
    }
    return (
        `https://www.quandl.com/api/v3/datasets/WIKI/${payload.tickerSymbol}.json?column_index=4&start_date=${
            payload.timeRange.start_date
        }&end_date=${payload.timeRange.end_date}&api_key=` + apiKey
    );
};

function dataFetch(options) {
    return axios(options)
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if (error.response) {
                // error handle here
                console.log(error);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log('Error', error.message);
            }
            //console.log(error.config);
        });
}

function getData(payload) {
    const endpoint = getEnpoint(payload);
    const options = {
        url: endpoint,
        method: 'GET',
    };
    return dataFetch(options);
}

export default {
    getData,
};
